use std::sync::RwLock;

use clap::Parser;


pub static VERBOSE : RwLock<bool>= RwLock::new(false);


#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
pub struct Args {

    /// Path of the mmdb files (e.g. GeoLite2-City.mmdb and Geolite2-ASN.mmdb) 
    #[arg(short, long,required = true)]
    pub db_mmdb: Option<String>,

    /// Size of memoisation dictionnary
    #[arg(short, long, default_value_t = 1000)]
    pub memoisation: usize,

    /// Input file if not set stdin is used 
    #[arg(short, long)]
    pub input_file : Option<String>,
 
    /// Output file if not set stdout is used
    #[arg(short, long)]
    pub output_file : Option<String>,

    /// Default it's ordered so each line match of the source file but if -u is set, order is not important but the line number is still set in ouptut file
    #[arg(short, long, default_value_t = false)]
    pub unordered : bool,

    /// Verbose on stdout 
    #[arg(short, long, default_value_t = false)]
    pub verbose : bool,

    /// Clean file or search IP using regex
    #[arg(short, long, default_value_t = false)]
    pub search : bool,

}



/// Represents the optional arguments that can be passed to the program.
///
/// # Fields
///
/// * `database_folder` - The path of the mmdb files.
/// * `input_file` - The input file path. If not set, stdin is used.
/// * `output_file` - The output file path. If not set, stdout is used.
/// * `memo_limit` - The size of the memoisation dictionary.
/// * `unordered` - A boolean flag that determines if the output should be ordered.
/// * `stdin` - A boolean flag that indicates if stdin is used as the input source.
/// * `stdout` - A boolean flag that indicates if stdout is used as the output destination.
/// * `search` - A boolean flag that determines if the program should clean the file or search IP using regex.
#[derive(Debug)]
pub struct OptArg {
    pub database_folder: String,
    pub input_file: String,
    pub output_file: String,
    pub memo_limit: usize,
    pub unordered: bool,
    pub stdin: bool,
    pub stdout: bool,
    pub search: bool,
}

/// 
/// This function takes a reference to an `Args` struct as input and returns an `OptArg` struct.
/// It parses the command-line arguments provided by the user and populates the fields of the `OptArg` struct accordingly.
///
/// # Arguments
///
/// * `args` - A reference to an `Args` struct containing the command-line arguments.
///
/// # Returns
///
/// * `OptArg` - An `OptArg` struct containing the parsed command-line arguments.
///
/// # Examples
///
/// ```
/// use your_module::Args;
///
/// let args = Args {
///     db_mmdb: Some(String::from("path/to/mmdb")),
///     input_file: Some(String::from("input.txt")),
///     output_file: Some(String::from("output.txt")),
///     memoisation: 1000,
///     unordered: false,
///     verbose: true,
///     search: false,
/// };
///
/// let opt_arg = parse_arg_in_struct(&args);
/// ```
pub fn parse_arg_in_struct(args: &Args) -> OptArg {

    // verbose set 
    if args.verbose {
        let mut verbose = VERBOSE.write().unwrap();
        *verbose = true;
    }

    let mut database_folder = String::from("");
    let mut input_file = String::from("");
    let mut output_file = String::from("");
    let mut stdin = false;
    let mut stdout = false;

    // required so no need to check
    if let Some(db_mmdb) = args.db_mmdb.as_deref() {
        database_folder = db_mmdb.to_string();
    }

    if let Some(input_file_arg) = args.input_file.as_deref() {
        input_file = input_file_arg.to_string();
    }
    else{
        stdin = true;
    }

    if let Some(output_file_arg) = args.output_file .as_deref(){  
        output_file = output_file_arg.to_string();
    }
    else{
        stdout = true;
    }
    

    OptArg {
        database_folder: database_folder,
        input_file: input_file,
        output_file: output_file,
        memo_limit: args.memoisation,
        unordered: args.unordered ,
        stdin: stdin,
        stdout: stdout,
        search: args.search,
    }


}