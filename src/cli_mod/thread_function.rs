//! # thread_function ( thread function )
//! 
//! simple mod to create thread and handle data from it
//!
//! 


use core::fmt;
use std::sync::{mpsc,Arc};
use std::thread;
use std::fs::{self, File};
use std::io::{self, BufRead, BufReader, Write};



use crate::cli_mod::maxmind_reader;
use crate::cli_mod::output_formatter;
use crate::cli_mod::func_help;
use crate::cli_mod::memoisation;

use super::search::search_ip;

/// Represents a message that can be sent between threads in the application.
///
/// # Fields
///
/// * `content` - An optional string that contains the main content of the message.
/// * `maxmind_data` - An optional string that contains additional data retrieved from a MaxMind database.
/// * `search` - A boolean value that indicates whether the message is a search request.
/// * `line_id` - A usize value that represents the line number or identifier of the message.
/// * `terminate` - A boolean value that indicates whether the message is a termination signal.
#[derive(Debug)]
pub struct Message {
    pub content: Option<String>,
    pub maxmind_data: Option<String>,
    pub search: bool,
    pub line_id: usize,
    pub terminate: bool,
}

impl fmt::Display for Message {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let default = String::from("");
        write!(f, "{},{},{}", self.line_id, self.content.as_ref().unwrap_or_else(|| &default), self.maxmind_data.as_ref().unwrap_or_else(|| &default))
    }
    
}

impl Message {
    pub fn shutdown() -> Message {
        Message{content: Option::None, maxmind_data: Option::None,search:false,line_id:0,terminate:true}
    }
}


/// Represents the source of content that can be read from either standard input or a file.
///
/// # Fields
///
/// * `from_stdin` - A boolean value that indicates whether the content should be read from standard input.
/// * `from_file` - An optional `BufReader<File>` that represents the file from which the content should be read.
///
/// # Examples
///
/// ```
/// use std::fs::File;
/// use std::io::BufReader;
/// use my_module::SourceContent;
///
/// // Reading from standard input
/// let source = SourceContent { from_stdin: true, from_file: None };
///
/// // Reading from a file
/// let file = File::open("example.txt").unwrap();
/// let reader = BufReader::new(file);
/// let source = SourceContent { from_stdin: false, from_file: Some(reader) };
/// ```
pub struct SourceContent {
    pub from_stdin: bool,
    pub from_file : Option<BufReader<File>>,
}

impl SourceContent {
    pub fn get_content(&mut self) -> Option<String> {
        let mut buffer = String::new();
        if self.from_stdin {
            match io::stdin().read_line(&mut buffer) {
                Ok(n) => {
                    if n > 0 { Some(buffer )} else { Option::None }
                }
                Err(_) => Option::None
            }
        }
        else{
            match self.from_file.as_mut().unwrap().read_line(&mut buffer) {
                Ok(n) => {
                    if n > 0 { Some(buffer )} else { Option::None }
                }
                Err(_) => Option::None
            }
        }
    }

}

/// get_db_threat
/// 
///     Start the thread db 
/// 
/// #Arguments 
///     `db` - reference to a db reader 
///     `receiver` - referece to the receiver 
///     `writer_tx` - a sender to the writer function
///     `limit_memo` - limit for memoisation
pub fn get_db_thread(db : Arc<maxmind_reader::ReaderDb>, receiver :  mpsc::Receiver<Message>, writer_tx : mpsc::Sender<Message>, limit_memo : usize )
    
    -> //(mpsc::Receiver<String>, thread::JoinHandle<i32>)
        thread::JoinHandle<i32>

{
    
    //let (writer_tx,writer_rx) = mpsc::channel();    
     
    let mut ip_to_data = memoisation::get_new_memoisation(limit_memo).unwrap();

    
    // assume the maxmind reader is open and sure
    
     thread::spawn(move || {
        

        for mut data in receiver {
            
            if data.terminate {
                writer_tx.send(Message::shutdown()).unwrap();
                return 0;
            }

            if data.search {
                let content = String::from(&data.content.unwrap());
                for line in search_ip(&content){
                    // do stuff and send to sender_tx
                    let mut data_2 = Message{content: Option::from(line), maxmind_data: Option::None,search:false,line_id:data.line_id,terminate:false};
                    match ip_to_data.key_exist(&data_2.content.clone().unwrap()) {
                        Some(s) => {
                            // data exist so send it
                            data_2.maxmind_data = Option::from(s.to_string());
                            writer_tx.send(data_2).unwrap();
                        }
                        None => {
                            // no key so add it 
                            match db.retrieve_data(&data_2.content.clone().unwrap()){
                                Ok(s) => {
                                    // IP is correct and found in base
                                    ip_to_data.add_data( data_2.content.clone().unwrap().as_ref(),&s.to_string());
                                    data_2.maxmind_data = Option::from(s.to_string());
                                    writer_tx.send(data_2).unwrap();
            
                                },
                                Err(_e) => {
                                    
                                    // ip is bad..
                                    data_2.maxmind_data = Option::from(maxmind_reader::get_generic_data().to_string());
                                    writer_tx.send(data_2).unwrap();
                
                                },
                            }
                        }
                    }
                }
            } 
            else{
                // do stuff and send to sender_tx
                match ip_to_data.key_exist(&data.content.clone().unwrap()) {
                    Some(s) => {
                        // data exist so send it
                        
                        data.maxmind_data = Option::from(s.to_string());
                        writer_tx.send(data).unwrap();
                    }
                    None => {
                        // no key so add it 
                        match db.retrieve_data(&data.content.clone().unwrap()){
                            Ok(s) => {
                                // IP is correct and found in base
                                ip_to_data.add_data( data.content.clone().unwrap().as_ref(),&s.to_string());
                                data.maxmind_data = Option::from(s.to_string());
                                writer_tx.send(data).unwrap();
        
                            },
                            Err(_e) => {
                                
                                // ip is bad..
                                // print_error_no_help(&e);
                                data.maxmind_data = Option::from(maxmind_reader::get_generic_data().to_string());
                                writer_tx.send(data).unwrap();
            
                            },
                        }
                    }
                }
            }




        }
        0
    })
    
    
}

/// get_writer_threat_file
/// 
///     Start the writer threat for output and return an handler
/// 
/// #Arguments 
///     `file` - file where to write the output if None stdout is the default output
///     `receiver` -  receiver channel
///     `order mode` - how many threads had to wait before closing false = ordered 1 thread and true = unordered 2 thread
pub fn get_writer_thread_universal(file : Option<fs::File> , receiver : mpsc::Receiver<Message>, unordered : bool)
        ->  thread::JoinHandle<i32>   
{

    let nb_threads = if unordered { 2 } else { 1 };
    
    if let Some(mut f) = file {
        // assume file is open and checked
        thread::spawn(move || {
            let mut shutdown = 0;

            // write a header to file ( CSV )
            match output_formatter::write_data( &mut f , &maxmind_reader::header_data_result()) {
                Ok(_) => {},
                Err(e) => {
                    func_help::print_error(&e);
                }
            }
            
            for data in receiver {
                
                if data.terminate {           
                    shutdown += 1;

                    if shutdown == nb_threads {
                        return 0;
                    }
                    continue;
                }
             
                match output_formatter::write_data(&mut f ,&data.to_string()) {
                            Ok(_) => {},
                            Err(e) => {
                                // fail to write the data
                                func_help::print_error(&e);
                            }
                }
                
            }
            0          
        })
    
    }
    else{
        thread::spawn(move || {
            let mut shutdown = 0;
    
            // write a header to stdout
            match io::stdout().write_all(&maxmind_reader::header_data_result().as_bytes()) {
                Ok(_) => {},
                Err(_) => {
                    func_help::print_error("Error with stdout");           
                }
            }
            
            for data in receiver {
                
                if data.terminate {
                    shutdown += 1; 
                    if shutdown == nb_threads {
                        return 0;
                    }
                    continue;
                }                  
                match io::stdout().write_all(&data.to_string().as_bytes()){
                    Ok(_) => {},
                    Err(_) => {
                        func_help::print_error("Error with stdout");           
                    }
                }  
            }
            0   
        })
    }

    
    
}