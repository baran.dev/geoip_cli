//! # input-_file_reader ( output function )
//! 
//! simple mod to create an handler for a file
//! and function to read it 
//! 



use std::fs::File;
use std::io::BufReader;




/// open a file and return a result with a BufReader on a File
/// 
/// # Arguments
///    *    `file`  - A string slice reference to the filename
/// 
/// # Remarks
///     Result<BufReader<File>,String> - Result with two option BufReader<File> 
///     if succeed and String if fail
///     BufReader function are ready to read files...
///     
/// 
/// # Example
/// 
/// ```
/// let file = "my_file";
/// 
/// match input_file_reader(&file) {
///     
///     Ok(o) => { // get the result },
///     Err(e) => { // error output 
///         println!("{}",e);
///         // ...
///     }
/// ```
pub fn input_file_reader(file : &str) -> Result<BufReader<File>,String> {

    let f = File::open(file);

    match f {
        Ok(b) => {
            let buf_reader = BufReader::new(b);
             Ok(buf_reader)
        },
        Err(e) => {
            let err = format!("Can t open file {} : {} ", file, e);
            Err(err)
        }

    }

}