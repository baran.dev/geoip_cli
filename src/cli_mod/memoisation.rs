//! memoisation module
//!
//! Little module to handle the limit of memoisation



use std::collections::BTreeMap;
use std::collections::VecDeque;


/// Memoisation
///
/// A struct that handles memoisation with a limit size.
///
/// Data is stored in a BTreeMap. A clone of the key is also stored in a VecDeque.
/// When the size limit is reached, the oldest key is removed from the VecDeque
/// and used to remove the corresponding key-value pair from the BTreeMap.
///
/// # Fields
///
/// * `data` - A BTreeMap that stores the memoised data.
/// * `queue` - A VecDeque that stores a clone of the keys in the order they were added.
/// * `limit` - The maximum size of the memoisation.
#[derive(Debug)]
pub struct Memoisation{ 
    
    pub data : BTreeMap<String,String>,
    pub queue : VecDeque<String>,
    pub limit : usize,
}



/// get_new_memoisation
///     
/// Get a struct who handle a memoisation with
/// a limit size
///
/// data is stored in a BTreeMap
/// a clone of key is on a VecDequeue
/// when the size is reached we remove a key from VecDequeue and use 
/// it to remove one key from BTreeMap
/// 
/// #Arguments
///     `size` - give the maximum size of the memoisation
/// 
pub fn get_new_memoisation(size : usize )  -> Result<Memoisation,&'static str> 
{
        
    if size == 0 {
        return Err("size is not valid for memoisation");
    }
        
    let data : BTreeMap<String,String> = BTreeMap::new();
    let queue : VecDeque<String> = VecDeque::with_capacity(size);
        
    Ok(Memoisation {
            
        data ,
        queue ,
        limit : size,
            
    })
        
        
}


impl Memoisation {
    
    
    /// key_exists
    ///     
    ///     return the data from given key or None 
    ///     
    /// #Arguments 
    ///     `key` - a key to find 
    pub fn key_exist( &self , key : &str ) -> Option<String> {
        
        if self.data.contains_key(key) {
            return Some(self.data.get(key).unwrap().clone());
        }

        
        None
    }
    
    
    /// add_data
    /// 
    ///     add a data to the memoisation , do the check 
    ///     for the limit when is reached or not
    /// 
    /// 
    /// #Arguments
    ///     `key` - a key to store
    ///     `value` - value associate to the key
    /// 
    ///     
    pub fn add_data ( &mut self , key : &str , value : &str) {
        
        
        
        // add it 
        if self.data.len() < self.limit {
            

            self.data.insert(String::from(key),String::from(value));
            self.queue.push_front(String::from(key));
            
        }
        
        else{
            self.data.remove(&self.queue.pop_back().unwrap());
            //then add it 
            // there we reached the limit so don't need to touch 
            // the limit ...
            self.data.insert(String::from(key),String::from(value));
            self.queue.push_front(String::from(key));
            
            
        }
        
        
        
    }
    
    
}