//! # func help
//! 
//! simple mod to print error on stdout
//!
//! 
//! 
//! 

use crate::cli_mod::arg_parser_clap::VERBOSE;
/// print an error to stdout if VERBOSE is set to true by the user otherwise to stderr
/// 
/// # Arguments
///    *    `err`  - A string slice reference , which error to print in stdout
/// # Example
/// 
/// ```
/// let error = "This is an error";
/// print_error_no_help(&error);
/// ```
/// 
/// 
pub fn print_error(err : &str){
    if VERBOSE.read().unwrap().to_owned() {
        println!("Out:---{}---" , err);
    }
}