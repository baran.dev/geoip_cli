//! # output_formatter ( output function )
//! 
//! simple mod to create an handler for a file
//! and function to write on it 
//! 



use std::fs::File;
use std::io::prelude::*;




/// open a file and return a result with File on it
/// 
/// # Arguments
///    *    `file`  - A string slice reference to the file
/// 
/// # Remarks
///     Result<File, String> - Result with two option File 
///     if succeed and String if fail
///     
/// 
/// # Example
/// 
/// ```
/// let file = "my_file";
/// 
/// match open_file_for_output(&file) {
///     
///     Ok(o) => { // get the result },
///     Err(e) => { // error output 
///         println!("{}",e);
///         // ...
///     },    File 
/// 
pub fn open_file_for_output(file : &str) -> Result<File, String> {


        let write_file = File::create(&file);

        match write_file {
            Ok(f) => {
                Ok(f)
            },
            Err(e) => Err(format!("Can t open : {} => {}",file,e))
        }

}



/// write a String on a given file 
/// 
/// # Arguments
///    *    `&mut file`  - A mutable reference to a File type
///    *    `&String` -  The content to write
/// 
/// # Remarks
///     Result<(), String> - Result with two option ()
///     if succeed and String if fail
///     There's no panic 
///     
/// 
/// # Example
/// 
/// ```
/// let file : File = ...;
/// let contents : String = String::from("...");
/// 
/// match write_data(&file) {
///     
///     Ok(o) => { // No result just an ok  },
///     Err(e) => { // error writing 
///         println!("{}",e);
///         // ...
///     },    
/// 
/// }
/// ```
/// 
pub fn write_data(file : &mut File , data : &str) -> Result<() , String>{

    match file.write_all(&(data.as_bytes())) {

        Ok(_) => Ok(()) ,
        Err(e) => {
                Err(format!("Can t write to file {}",e))
        }
    }
    

}

