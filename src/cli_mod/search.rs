

use {
     once_cell::sync::Lazy, regex::Regex
};

static RE_IPV4 : Lazy<Regex> = Lazy::new(|| Regex::new(r"((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)").unwrap());
static RE_IPV6 : Lazy<Regex> = Lazy::new(|| Regex::new(r"(?:(?:[A-Fa-f0-9]{1,4}:){7}[A-Fa-f0-9]{1,4}|(?:[A-Fa-f0-9]{1,4}:){1,7}:|(?:[A-Fa-f0-9]{1,4}:){1,6}:[A-Fa-f0-9]{1,4}|(?:[A-Fa-f0-9]{1,4}:){1,5}(?::[A-Fa-f0-9]{1,4}){1,2}|(?:[A-Fa-f0-9]{1,4}:){1,4}(?::[A-Fa-f0-9]{1,4}){1,3}|(?:[A-Fa-f0-9]{1,4}:){1,3}(?::[A-Fa-f0-9]{1,4}){1,4}|(?:[A-Fa-f0-9]{1,4}:){1,2}(?::[A-Fa-f0-9]{1,4}){1,5}|[A-Fa-f0-9]{1,4}:(?:(?::[A-Fa-f0-9]{1,4}){1,6})|:(?:(?::[A-Fa-f0-9]{1,4}){1,7}|:)|fe80:(?::[A-Fa-f0-9]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(?:ffff(?::0{1,4}){0,1}:){0,1}[\da-fA-F]{1,4}:[\da-fA-F]{1,4}|(?::[\da-fA-F]{1,4}){1,4}:[\da-fA-F]{1,4}|(?::[\da-fA-F]{1,4}){1,3}:[\da-fA-F]{1,4}:[\da-fA-F]{1,4}|(?::[\da-fA-F]{1,4}){1,2}:[\da-fA-F]{1,4}:[\da-fA-F]{1,4}:[\da-fA-F]{1,4}|(?::[\da-fA-F]{1,4}){1,1}:[\da-fA-F]{1,4}:[\da-fA-F]{1,4}:[\da-fA-F]{1,4}:[\da-fA-F]{1,4}|[\da-fA-F]{1,4}:[\da-fA-F]{1,4}:[\da-fA-F]{1,4}:[\da-fA-F]{1,4}:[\da-fA-F]{1,4}:[\da-fA-F]{1,4}:[\da-fA-F]{1,4}|[\da-fA-F]{1,4}:[\da-fA-F]{1,4}:[\da-fA-F]{1,4}:[\da-fA-F]{1,4}:[\da-fA-F]{1,4}:[\da-fA-F]{1,4}:[\da-fA-F]{1,4}:[\da-fA-F]{1,4})").unwrap());




/// Searches for IPv4 and IPv6 addresses in a given text and returns them as a vector of strings.
///
/// # Arguments
///
/// * `text` - A string slice that contains the text to search for IP addresses.
///
/// # Returns
///
/// * A vector of strings that contains all the IP addresses found in the text.
///
/// # Examples
///
/// ```
/// use ip_address_search::search_ip;
///
/// let text = "This is a test string with IPv4 address 192.168.1.1 and IPv6 address 2001:0db8:85a3:0000:0000:8a2e:0370:7334.";
/// let ips = search_ip(text);
/// assert_eq!(ips, vec!["192.168.1.1", "2001:0db8:85a3:0000:0000:8a2e:0370:7334"]);
/// ```
pub fn search_ip(text: &str) -> Vec<String> {
    let mut res = vec![];

    RE_IPV4.find_iter(text).for_each(|m| res.push(m.as_str().to_string()));
    RE_IPV6.find_iter(text).for_each(|m| res.push(m.as_str().to_string()));

    res
}