//! # maxmind_readers ( mod to get data from maxmind db)
//! 
//! simple mod to create an handler for different maxmind db 
//! and function to read , retrieve data
//! 
//! 

use maxminddb::geoip2;
use maxminddb::Reader;
use std::net::IpAddr;
use std::string::ToString;
use std::str::FromStr;

/// Struct to store data from maxmind db 
/// If you want to add more data you can add it there 
/// modify header_data_result function
/// modify get_generic_data
/// modify 
pub struct DataResult {
    pub country : String,
    pub longitude : f64,
    pub latitude : f64,
    pub asn : u32,
    pub isp : String,
}



/// Return a simple structure to put on the
/// head of csv file 
/// 
/// In this case the return is a string
/// IP , Country , Longitude , Latitude , ASN , isp \n
///
pub fn header_data_result() -> String {
    String::from("Line , IP , Country , Longitude , Latitude , ASN , ISP \n")
}


/// Return a generic_data for country , long, lat , asn and isp
/// 
/// Useful when we can t find data about certain ip 
/// 
/// #Arguments 
///     `ip` - reference to an ip 
/// 
pub fn get_generic_data() -> DataResult {

    let generic : DataResult = DataResult {
        country : String::from("None"),
        longitude : 0.0,
        latitude : 0.0,
        asn : 0,
        isp : String::from("None"),
    };
    generic
}


/// Getting a string of the data in DataResult
impl ToString for DataResult {
        fn to_string(&self) -> String{
            format!("\"{}\",{},{},{},\"{}\"\n", self.country,self.longitude,self.latitude,self.asn,self.isp)
        }
}

/// ReaderDb is a structure who save city and isp file reader
/// handler 
pub struct ReaderDb {
    city : Reader<Vec<u8>>,
    isp : Reader<Vec<u8>>,
}

/// reader_db : function to get handler for city and isp database
/// 
/// #Arguments
///     `path`  - Path to the database folder
/// 
/// # Remarks 
///     We need the path to this two differents files
///     GeoLite2-City.mmdb
///     GeoLite2-ASN.mmdb
/// You can add more database just play with the struct on this file
pub fn reader_db( path : &str) -> Result<ReaderDb,String> {

    let db_city_loc = format!("{}/GeoLite2-City.mmdb",path);
    let db_isp_loc  = format!("{}/GeoLite2-ASN.mmdb",path);


    let reader_city = maxminddb::Reader::open_readfile(&db_city_loc);
    let reader_isp  = maxminddb::Reader::open_readfile(&db_isp_loc);


    let city  = match reader_city {
        Ok(r) => r ,
        Err(e) => return Err(format!("Maxmind error: {}",e.to_string())),
    };


    let isp = match reader_isp {
        Ok(r) => r ,
        Err(e) => return Err(format!("Maxmind error: {}",e.to_string())),
    };

    Ok(ReaderDb { city , isp })

}


impl ReaderDb {

    /// retrive_data function to get the information we need
    /// about an ip adress 
    /// if the search or bad ip , we get an error message
    /// 
    /// NO PANIC is this case
    /// 
    /// #Arguments 
    ///     `ip` - The ip we need information  about
    /// 
    pub fn retrieve_data(&self, ip : &str ) -> Result<DataResult,String>{

        // should never fail if ip is checked already
        let ipstr = String::from(ip);
        
        let ip : IpAddr;
        
        match FromStr::from_str(&ipstr) {
            Ok(o) => ip = o,
            Err(e) => {
                return Err(format!("IP : {} Error => {} ",ipstr,e));
            }
        }

        


        let city : geoip2::City = match self.city.lookup(ip){
            Ok(o) => o,
            Err(e) => return Err(format!("Maxmind error: {}",e.to_string())),
        };

        let isp : geoip2::Isp = match self.isp.lookup(ip) {
            Ok(o) => o,
            Err(e) => return Err(format!("Maxmind error: {}",e.to_string())),
        };
        

        let country : String = match city.country {
            Some(s) => {
                match s.names.unwrap().get("en") {
                    Some(s) => String::from(*s),
                    None => String::from("None")
                }
            },
            None => String::from("None"),
        };

        let (latitude,longitude) = match city.location {
            Some(s) => {
                (s.latitude.unwrap_or_else(|| 0.0),s.longitude.unwrap_or_else(|| 0.0))
            },
            None => (0.0,0.0),
        };

        let asn= match isp.autonomous_system_number {
            Some(s) => s,
            None => 0,
        };

        let isp_ = match isp.autonomous_system_organization {
            Some(s) => {
                String::from(s)
            },
            None => String::from("None"),
        };


        Ok(DataResult {
            country,
            longitude,
            latitude,
            asn,
            isp : isp_,
        })

    }

}