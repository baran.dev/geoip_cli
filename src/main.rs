use std::sync::{mpsc,Arc};
use clap::Parser;

mod cli_mod;
use cli_mod::func_help::print_error;
use cli_mod::input_file_reader;
use cli_mod::maxmind_reader;
use cli_mod::output_formatter;
use cli_mod::thread_function::{self, Message};
use cli_mod::func_help;

use cli_mod::arg_parser_clap::*;


fn main() {

    // bench :D 
    let now = std::time::Instant::now();

    let clap_args = Args::parse();

    let my_args = parse_arg_in_struct(&clap_args);
    
    /***************************
    ***GETTING MAXMIND DATABASE* 
    ***************************/
    let db_data : Arc<maxmind_reader::ReaderDb>;

    match maxmind_reader::reader_db(&my_args.database_folder) {
        Ok(o) => db_data = Arc::new(o),
        Err(e) => {
            func_help::print_error(&e);
            return;
        }
    }

    print_error("Maxmind DB GOOD");

    // content line 
    let mut contents = String::new();
    // channel for output 
    let (writer_tx, writer_rx) = mpsc::channel();
    // channel for database access reader... 
    let (db_tx, db_rx) = mpsc::channel();
  

    
    /****************************
    ***FIRST THREAD READER******* 
    Receive an input and retrieve
    data from maxmind database***
    ***************************/
    let db_jh = thread_function::get_db_thread(Arc::clone(&db_data),db_rx,writer_tx.clone(),my_args.memo_limit);
    

    // loop to get line by line the data in input file 
    // two case unordered we can create 2 threads for the reader process 
    // ordered one thread for the reader process is needed

    let mut line_id = 0;

    /****************************
    ***SET THREAD WRITER******* 
    ***write to stdout or file***
    ***************************/
    let writer_jh = {
        if my_args.stdout == false {
            // getting file for output
            let out_file = match output_formatter::open_file_for_output(&my_args.output_file){
                Ok(a) => a,
                Err(e) => { func_help::print_error(&e); return;}
            };
            //  thread output 
            thread_function::get_writer_thread_universal(Option::from(out_file),writer_rx,my_args.unordered)
        }
        else{
            thread_function::get_writer_thread_universal(Option::None,writer_rx,my_args.unordered)
        }
    };                   


    /***************************
    *** INPUT DATA SOURCE*******
    ***************************/
    let mut source_content;
    if my_args.stdin == true {
        print_error("DEFAULT INPUT: STDIN");
        source_content = thread_function::SourceContent {
            from_stdin : true,
            from_file : None,
        };
    }
    else {
        let my_buffer_input = match input_file_reader::input_file_reader(&my_args.input_file) {
            Ok(a) => a,
            Err(e) => {
                func_help::print_error(&e);
                return;      
            }
        };
        source_content = thread_function::SourceContent {
            from_stdin : false,
            from_file : Some(my_buffer_input),
        };
        print_error("DEFAULT INPUT: FROM FILE");
    }
    
    /***************************
    ***UNORDERED  MODE  ******** 
    ***************************/
    if my_args.unordered == true {     
        print_error("MODE: UNORDERED");
        // 2nd channel for multi thread
        let (db2_tx , db2_rx) = mpsc::channel();
        // thread two reader
        let db2_jh = thread_function::get_db_thread(Arc::clone(&db_data),db2_rx,writer_tx,my_args.memo_limit);
        
        let mut alternate = 0;    
    
           // getting buffer from stdin  
            loop {
                // get a line
                // we read a line from here 
                contents.clear();
                line_id += 1;
                match source_content.get_content() {
                    Some(s) => {
                        contents = s;
                    }
                    Option::None => {
                        db_tx.send(Message::shutdown()).unwrap();
                        db2_tx.send(Message::shutdown()).unwrap();
                        break;
                    },
                }
                        
                // removing \n 
                //contents.remove(contents.len()-1);
                contents.retain(|c| c != '\n');
                contents.retain(|c| c != '\r');
                
                // sending a clone 
                if alternate == 0 {
                    db_tx.send(
                        Message {
                            line_id: line_id,
                            content: Option::from(contents.to_string()),
                            search: my_args.search,
                            terminate: false,
                            maxmind_data: Option::None,
                        }
                    ).unwrap();
                    alternate = 1;
                }
                else{
                    db2_tx.send(
                        Message {
                            line_id: line_id,
                            content: Option::from(contents.to_string()),
                            search: my_args.search,
                            terminate: false,
                            maxmind_data: Option::None,
                        }
                    ).unwrap();
                    alternate= 0;
                }
            }
        // wait until all is finished 
        writer_jh.join().unwrap();    
        db_jh.join().unwrap();
        db2_jh.join().unwrap();

    }
    else{
        print_error("MODE: ORDERED");
        /***************************
        ***** ORDERED  MODE ******** 
        ***************************/
            loop {
                // get a line
                contents.clear();
                line_id += 1;

                match source_content.get_content() {
                    Some(s) => {
                        contents = s;
                    }
                    Option::None => {
                        db_tx.send(Message::shutdown()).unwrap();
                        break;
                    },
                }
                        
                // removing \n 
                //contents.remove(contents.len()-1);
                contents.retain(|c| c != '\n');
                contents.retain(|c| c != '\r');
                
                // sending a clone 
                db_tx.send(
                    Message {
                        line_id: line_id,
                        content: Option::from(contents.to_string()),
                        search: my_args.search,
                        terminate: false,
                        maxmind_data: Option::None,
                    }
                ).unwrap();
        
            }
        
        // wait until all is finished 
        writer_jh.join().unwrap();    
        db_jh.join().unwrap();
        
    }

    let elapsed_time = now.elapsed();
    print_error(format!("Elapsed time: {:?} ms",elapsed_time.as_millis()).as_str());
}

