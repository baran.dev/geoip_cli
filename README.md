# Installation

## Requirements

This project was tested on Linux with the latest version of the kernel
and rust compiler.

Install the latest rust version ( 2021 at least ).
https://rustup.rs/

## Run 

*Compile from source:*
```
Fork this repo then `cargo build --release`  

The binary is located there path_to_the_folder/target/release/geoip_cli
```
Download database from maxmind website 
https://dev.maxmind.com/geoip/geoip2/geolite2/

GeoLite2 ASN 

GeoLite2 City 

GeoLite2 Country 


FORMAT REQUIRED: mmdb ( just extract don't change the names)

## Usage

This is simple tool that take a list of ip or a raw file with ip address in it and try to find country , LAT/LONG , ASO and ASN of each ip address.

Output is formatted as CSV, easily readable. 

```bash 
Usage: geoip_cli [OPTIONS] --db-mmdb <DB_MMDB>

Options:
  -d, --db-mmdb <DB_MMDB>          Path of the mmdb files (e.g. GeoLite2-City.mmdb and Geolite2-ASN.mmdb)
  -m, --memoisation <MEMOISATION>  Size of memoisation dictionnary [default: 1000]
  -i, --input-file <INPUT_FILE>    Input file if not set stdin is used
  -o, --output-file <OUTPUT_FILE>  Output file if not set stdout is used
  -u, --unordered                  Default it's ordered so each line match of the source file but if -u is set, order is not important but the line number is still set in ouptut file
  -v, --verbose                    Verbose on stdout
  -s, --search                     Clean file or search IP using regex
  -h, --help                       Print help
  -V, --version                    Print version
```


### Output 
This is an example of output

Line | IP |Country | Longitude |Latitude | ASN | ASO 
---- | --- | -------------| ----------------| -------------| --------|------
1 | 5.39.70.218  | France |  2.3387000000000002  | 48.8582 | 16276 |  "OVH SAS"
2 | 23.121.211.21 |  United States |  -97.4757 |  35.6211 |  7018  | "AT&T Services, Inc."
3 | 23.129.64.100 | United States  | -122.3248 |  47.6008 | 396507 | "Emerald Onion"


## Changelogs 
* V0.1
	* simple mono thread with no memoisation limit 
* V0.2 
	* Added multi threading 
	* Memoisation limit DEFAULT
	* little improvement and fix
* V0.3
	* added two mode ordered and unordered
	* fix and improvements
	* doc fix
* V0.3.2
	* fix on arg parser

* V0.4
    * added STDIN/STDOUT input 
    * some fix 
* V0.5
    * added search mode (regex)
    * added verbose mode
    * fixed \r\n issue
    * fixed some issue with unordered mode
    * reduced memory footprint
    * code refactoring 
    * improvement of the argument parser


## TO DO 

* build the project to be self portable
* put on crates.io
* adapt the database reader for ipinfo mmdb


## Dependencies

    maxminddb => [link to crate](http://oschwald.github.io/maxminddb-rust/maxminddb/index.htm) 

# Multithreading 
	
## Case 1 ordered 
Main thread handle the cmd line arg and start two more threads.
Main thread send a line to thread 2.
Thread 2 try to resolve first with memoisation if not call maxminddb and save it in memoisation for futur resolve.
Send the result to thread 3 and simply write the result in the output file. 

## Case 2 unordered 

Main thread handle the cmd line arg and start 3 threads.
Then the main thread send line by line to thread 2 and 3 ( alternatively ) 
Thread 2  and 3 try to resolve first with memoisation if not call maxminddb and save it in memoisation for futur resolve.
Send the result to thread 4.
Thread 4 simply write the result in the output file. 

# Benchmark  

## Information

    These benchmark it's just for test purpose. 
    You can get different result. 

## Specs 
    AMD Ryzen 9 3900X
    Nvme drive
    Os : Kernel 6.6.47-1
    Cargo : cargo 1.80.0 (376290515 2024-07-16)
    Rustc : cargo 1.80.0 (376290515 2024-07-16)
    Profile : Release


## V0.5 Multi thread ordered/unordered and search mode

### 1_ 1_ Stats with memoisation RELEASE ordered 

    200K ip list with invalid ip etc
    ( there are several times the same ip )

Output on file 
| Search on    | Search off |
| -------- | ------- |
| 1139 ms |  1150 ms |

Output on stdout

| Search on    | Search off |
| -------- | ------- |
| 704 ms |  623 ms |

### 1_ 2_ Stats with memoisation RELEASE unordered 

    200K ip list with invalid ip etc ( they are a lot of same ip )

Output on file 
| Search on    | Search off |
| -------- | ------- |
| 1134 ms |  1176 ms |

Output on stdout

| Search on    | Search off |
| -------- | ------- |
| 634 ms |  635 ms |



## LICENCE 

It's free software, you can modify, use and ditribute for free.
Do not forget to add the original creator in the rights for other projects.
If you have made some improvement of this software you can share
your code with the community. 

### Thanks
    Thanks to oschwald for his work on maxminddb interface for us.